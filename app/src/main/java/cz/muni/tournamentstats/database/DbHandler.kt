package cz.muni.tournamentstats.database

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import cz.muni.tournamentstats.matches.Match

class DbHandler (private val context: Context) :SQLiteOpenHelper(
    context,
    DATABASE_NAME, null,
    DB_VERSION
) {
    override fun onCreate(db: SQLiteDatabase) {
        val createMatchTable = "CREATE TABLE $TABLE_MATCH (" +
                "$COL_ID integer PRIMARY KEY AUTOINCREMENT," +
                "$COL_MATCH_CREATED_AT datetime DEFAULT CURRENT_TIMESTAMP," +
                "$COL_MATCH_NAME varchar);"
        db.execSQL(createMatchTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        // TODO: Migrations
    }

    /**
     * Adds a [Match] entry to the database or updates an existing one.
     */
    fun addOrUpdateMatch(match: Match) {
        val db = writableDatabase
        val cv = ContentValues()

        cv.put(COL_MATCH_NAME, match.name)

        if (match.id != -1L) cv.put(COL_ID, match.id)
        val id = db.insertWithOnConflict(TABLE_MATCH, null, cv, SQLiteDatabase.CONFLICT_IGNORE)
        if (id == -1L) {
            db.update(TABLE_MATCH, cv, "$COL_ID = ?", arrayOf(match.id.toString()))
        }
    }
}