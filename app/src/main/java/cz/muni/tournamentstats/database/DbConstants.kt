package cz.muni.tournamentstats.database

const val DATABASE_NAME = "MatchList"
const val DB_VERSION = 1

const val COL_ID = "id"


const val TABLE_MATCH = "Match"
const val COL_MATCH_CREATED_AT = "createdAt"
const val COL_MATCH_NAME = "name"

/*
const val TABLE_MOVIE_ITEM = "MovieItem"
const val COL_MOVIE_ITEM_CATEGORY_ID = "categoryId"
const val COL_MOVIE_ITEM_NAME = "itemName"
const val COL_MOVIE_ITEM_CREATED_AT = "createdAt"
const val COL_MOVIE_ITEM_WATCHED = "watched"
const val COL_MOVIE_ITEM_ORDER = "customOrder"
*/