package cz.muni.tournamentstats.matches.tabs

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Rect
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.PixelCopy
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.google.android.material.floatingactionbutton.FloatingActionButton
import cz.muni.tournamentstats.R
import cz.muni.tournamentstats.players.Player
import java.io.File
import java.io.FileOutputStream
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [StatFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class StatFragment : Fragment() {

    companion object {
        private const val DoublesKey = "doubles"

        fun newInstance(doubles: Boolean) = StatFragment().apply {
            arguments = Bundle().apply {
                putBoolean(DoublesKey, doubles)
            }
        }
    }

    private var doublesMatch: Boolean = false
    private val sharedModel: SharedMatchModel by activityViewModels()

    private lateinit var timeView: LinearLayout
    private lateinit var playerStatViews: List<LinearLayout>

    private lateinit var timeUpdater: Handler

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.player_stat_fragment, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.getBoolean(DoublesKey)?.let {
            doublesMatch = it
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        playerStatViews = mutableListOf(
            view.getPlayerLayout(R.id.playerA1stat),
            view.getPlayerLayout(R.id.playerA2stat),
            view.getPlayerLayout(R.id.playerB1stat),
            view.getPlayerLayout(R.id.playerB2stat))
        timeView = view.findViewById(R.id.layout_time)
        if (!doublesMatch) {
            hidePlayers(1, 3)
            sharedModel.match.hidePlayers(1, 3)
        }
    }

    override fun onResume() {
        super.onResume()
        initTimeUpdaterOnce()
        playerStatViews.forEachIndexed { i, playerStatInfo ->
            val sourcePlayer = sharedModel.match.players[i]
            if (sourcePlayer.visible) {
                playerStatInfo.updatePlayerName(sourcePlayer)
                playerStatInfo.updatePlayerStats(sourcePlayer)
            }
        }
    }

    /**
     * Initializes the time updater as soon as the game starts
     */
    private fun initTimeUpdaterOnce() {
        if (!this::timeUpdater.isInitialized && sharedModel.match.hasStarted) {
            timeView.findViewById<TextView>(R.id.text_time_start_display)
                .text = sharedModel.match.displayTime(sharedModel.match.startTime)

            timeUpdater = Handler()
            val delay = 1000 // milliseconds
            timeUpdater.postDelayed(object : Runnable {
                override fun run() {
                    if (!sharedModel.match.isPaused) updateTimerDisplay()
                    if (!sharedModel.match.hasFinished) {
                        timeUpdater.postDelayed(this, delay.toLong())
                    }
                    else {
                        timeView.findViewById<TextView>(R.id.text_time_end_display)
                            .text = sharedModel.match.displayTime(sharedModel.match.endTime)
                        updateTimerDisplay()
                    }
                }
            }, delay.toLong())
        }
    }

    /**
     * Hides the inactive [Player] from the layout.
     */
    private fun hidePlayers(vararg indexes: Int) {
        for (i in indexes) {
            val playerLayout = playerStatViews[i]
            playerLayout.visibility = View.GONE
        }
    }

    private fun updateTimerDisplay() {
        timeView.findViewById<TextView>(R.id.text_time_elapsed_display)
            .text = sharedModel.match.displayElapsedTime()
    }

    private fun LinearLayout.updatePlayerName(sourcePlayer: Player) {
        this.findViewById<TextView>(R.id.player_name)
            .text = sourcePlayer.name
    }

    private fun LinearLayout.updatePlayerStats(sourcePlayer: Player) {
        this.findViewById<TextView>(R.id.text_1S_display)
            .text = sourcePlayer.displayAccuracyPercentage(Player.Serve.FIRST)
        this.findViewById<TextView>(R.id.text_1S_value_display)
            .text = sourcePlayer.displayAccuracyCount(Player.Serve.FIRST)
        this.findViewById<TextView>(R.id.text_2S_display)
            .text = sourcePlayer.displayAccuracyPercentage(Player.Serve.SECOND)
        this.findViewById<TextView>(R.id.text_2S_value_display)
            .text = sourcePlayer.displayAccuracyCount(Player.Serve.SECOND)
        this.findViewById<TextView>(R.id.text_df_display)
            .text = sourcePlayer.doubleFaults.toString()
        this.findViewById<TextView>(R.id.text_win_display)
            .text = sourcePlayer.winningBalls.toString()
    }

    /**
     * Finds the indivitual player layout.
     */
    private fun View.getPlayerLayout(id: Int) : LinearLayout {
        return this.findViewById(id) as LinearLayout
    }
}