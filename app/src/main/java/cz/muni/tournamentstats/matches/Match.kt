package cz.muni.tournamentstats.matches

import cz.muni.tournamentstats.players.Player
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


data class Match(var name:String,
                 var doubles:Boolean = false) {
    var id:Long = -1
    override fun toString(): String = name

    enum class Phase { PREPARING, RUNNING } // FINISHED unecessary
    var phase = Phase.PREPARING
        private set

    private fun advancePhase() {
        phase = phase.next()
    }

    // Timer
    lateinit var startTime: Timestamp
        private set
    lateinit var endTime: Timestamp
        private set
    val hasStarted: Boolean
        get() = this::startTime.isInitialized
    val hasFinished: Boolean
        get() = this::endTime.isInitialized

    private var pauseTimeMillis:Long = 0
    private lateinit var pauseStartTime: Timestamp
    var isPaused: Boolean = false
        private set

    private val elapsedTimeMillis: Long
        get() {
            return if (hasStarted && hasFinished)   // Finished Match
                (endTime.time - startTime.time) - pauseTimeMillis
            else if (hasStarted && !hasFinished)    // Running Match
                (Timestamp(System.currentTimeMillis()).time - startTime.time) - pauseTimeMillis
            else 0                                  // Match hasn't started yet
        }


    // Sets
    val sets = arrayOfNulls<Player.Team>(5)
    lateinit var gameResult: Player.Team

    // Players
    val players: List<Player> = listOf(
        Player("Hráč 1 (A)", Player.Team.A), Player("Hráč 2 (A)", Player.Team.A),
        Player("Hráč 1 (B)", Player.Team.B), Player("Hráč 2 (B)", Player.Team.B))

    fun displayElapsedTime(): String {
//        val hours = elapsedTimeMillis / 1000 / 60 / 60
//        val minutes = (elapsedTimeMillis / 1000 / 60 / 60 - hours) * 60
//        val seconds: Long = ((elapsedTimeMillis / 1000 / 60 / 60 - hours) * 60 - minutes) * 60
        val hours = TimeUnit.MILLISECONDS.toHours(elapsedTimeMillis)
        val minutes = TimeUnit.MILLISECONDS.toMinutes(elapsedTimeMillis) % 60
        val seconds = TimeUnit.MILLISECONDS.toSeconds(elapsedTimeMillis) % 60
        val s = if (seconds < 10) "0${seconds}" else "$seconds"
        val m = if (minutes < 10) "0${minutes}" else "$minutes"
        val h = if (hours < 10) "0${hours}" else "$hours"
        return "$h:$m:$s"
    }

    fun displayTime(timeStamp: Timestamp, format: String = "HH:mm") : String {
        return SimpleDateFormat(format, Locale.getDefault()).format(timeStamp)
    }

    fun start() {
        startTime = Timestamp(System.currentTimeMillis())
        advancePhase()
    }
    fun pause() = switchPause(true)
    fun unpause() = switchPause(false)
    private fun switchPause(on: Boolean) {
        isPaused = on
        if (on) {
            pauseStartTime = Timestamp(System.currentTimeMillis())
        } else {
            val pauseEndTime = Timestamp(System.currentTimeMillis())
            pauseTimeMillis += (pauseEndTime.time - pauseStartTime.time)
        }
    }
    fun stop() {
        if (this::startTime.isInitialized) {
            endTime = Timestamp(System.currentTimeMillis())
            advancePhase()
        }
    }

    /**
     * Changes the name of a [Player] with index 0..4.
     */
    fun renamePlayer(index: Int, name: String) {
        players[index].name = name
    }

    /**
     * Sets the inactive [Player] flags to hidden.
     */
    fun hidePlayers(vararg indexes: Int) {
        for (i in indexes) {
            players[i].visible = false
        }
    }

    /**
     * Gets the next value of an [Enum].
     */
    inline fun <reified T: Enum<T>> T.next(): T {
        val values = enumValues<T>()
        val nextOrdinal = if (ordinal + 1 < values.size) ordinal + 1 else ordinal
        return values[nextOrdinal]
    }

}