package cz.muni.tournamentstats.matches.tabs


import androidx.lifecycle.ViewModel
import cz.muni.tournamentstats.matches.Match

class SharedMatchModel : ViewModel() {
    // TODO: Implement the ViewModel

    var doubles = false;

    val match = createMatch(doubles)

    private fun createMatch(doubles: Boolean, name: String? = null) : Match {
        // TODO: Create Match
        return if (name == null) Match("Zápas", doubles)
        else Match(name, doubles)
    }
}