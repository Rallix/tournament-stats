package cz.muni.tournamentstats.matches.tabs

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getColor
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import cz.muni.tournamentstats.players.Player
import cz.muni.tournamentstats.R
import cz.muni.tournamentstats.matches.Match


/**
* First tab of the core activity.
*/
class PlayerFragment : Fragment() {

    companion object {
        private const val DoublesKey = "doubles"

        fun newInstance(doubles: Boolean) = PlayerFragment().apply {
            arguments = Bundle().apply {
                putBoolean(DoublesKey, doubles)
            }
        }
    }

    private var doublesMatch: Boolean = false
    private val sharedModel: SharedMatchModel by activityViewModels()

    private lateinit var playerViews: List<LinearLayout>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.player_fragment, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.getBoolean(DoublesKey)?.let {
            doublesMatch = it
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedModel.doubles = doublesMatch

        playerViews = mutableListOf(
            view.getPlayerLayout(R.id.playerA1),
            view.getPlayerLayout(R.id.playerA2),
            view.getPlayerLayout(R.id.playerB1),
            view.getPlayerLayout(R.id.playerB2))
        view.lockButtons(true) // lock buttons before the match starts
        // Rename all players
        playerViews.forEachIndexed { i, playerInfo ->
            playerInfo.findViewById<EditText>(R.id.player_name_text)
                .initPlayerNameEditText(i, view)
            val player = sharedModel.match.players[i]
            playerInfo.findViewById<ImageView>(R.id.player_1S_yes).initPlayer1SYes(player)
            playerInfo.findViewById<ImageView>(R.id.player_1S_no).initPlayer1SNo(player)
            playerInfo.findViewById<ImageView>(R.id.player_2S_yes).initPlayer2SYes(player)
            playerInfo.findViewById<ImageView>(R.id.player_2S_no).initPlayer2SNo(player)
            playerInfo.findViewById<ImageView>(R.id.player_1S_undo).initPlayer1SUndo(player)
            playerInfo.findViewById<ImageView>(R.id.player_2S_undo).initPlayer2SUndo(player)
            playerInfo.findViewById<ImageView>(R.id.player_win).initPlayerWin(player)
        }
        if (!doublesMatch) {
            hidePlayers(1, 3)
            sharedModel.match.hidePlayers(1, 3)
        }

        // Floating timer button
        view.findViewById<FloatingActionButton>(R.id.fab_match_timer).initFloatingStartTimer(view)
        view.findViewById<FloatingActionButton>(R.id.fab_match_timer_stop)?.apply {
            this.initFloatingStopTimer(view)
            this.visibility = View.INVISIBLE
        }

    }

    /**
     * Finds the indivitual player layout.
     */
    private fun View.getPlayerLayout(id: Int) : LinearLayout {
        return this.findViewById(id) as LinearLayout
    }

    /**
     * Hides the inactive [Player] from the layout.
     */
    private fun hidePlayers(vararg indexes: Int) {
        for (i in indexes) {
            val playerLayout = playerViews[i]
            playerLayout.visibility = View.GONE
        }
    }

    /**
     * Returns a numbered name of a [Player].
     */
    private fun getNumberedPlayer(view: View, index: Int): String {
        var basicText = view.context.getString(R.string.player)
        basicText = basicText.replace("#", (index % 2 + 1).toString())
        val number = index % 2 + 1
        val team = if (index < 2) 'A' else 'B'
        return "$basicText $number ($team)"
    }

    // Buttons -------------------------------------------------------------------

    private fun FloatingActionButton.initFloatingStartTimer(view: View) {
        val matchType = if (doublesMatch) getString(R.string.match_doubles) else getString(R.string.match_singles)
        this.setOnClickListener { v ->
            when (sharedModel.match.phase) {
                Match.Phase.PREPARING -> {
                    //! Start

                    // Start timer, change icon
                    sharedModel.match.start()
                    view.lockButtons(false)

                    this.setImageDrawable(ContextCompat.getDrawable(v.context, R.drawable.ic_media_pause))
                    view.findViewById<FloatingActionButton>(R.id.fab_match_timer_stop).visibility = View.VISIBLE

                    // Lock EditText
                    playerViews.forEach { playerInfo ->
                        playerInfo.findViewById<EditText>(R.id.player_name_text).isEnabled = false
                    }

                    // Display 'START'
                    val startTime = sharedModel.match.displayTime(sharedModel.match.startTime)
                    val msg = "${getString(R.string.time_start)}: $matchType ($startTime)"
                    Snackbar.make(view, msg, Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show()
                }
                Match.Phase.RUNNING -> {
                    if (!sharedModel.match.isPaused) {
                        //! Pause
                        sharedModel.match.pause()
                        view.lockButtons(true)
                        this.setColorFilter(v.context.getColor(R.color.colorWin))
                    } else {
                        //! Unpause
                        sharedModel.match.unpause()
                        view.lockButtons(false)
                        this.setColorFilter(v.context.getColor(R.color.colorWhite))
                    }
                }
            }
        }
    }
    private fun FloatingActionButton.initFloatingStopTimer(view: View) {
        val matchType = if (doublesMatch) getString(R.string.match_doubles) else getString(R.string.match_singles)
        this.setOnClickListener {
            Snackbar.make(view, getString(R.string.hold_to_stop), Snackbar.LENGTH_SHORT)
                .setAction("Action", null).show()
        }
        this.setOnLongClickListener { v ->
            if (sharedModel.match.phase != Match.Phase.RUNNING) return@setOnLongClickListener true
            //! Stop
            this.setColorFilter(v.context.getColor(android.R.color.holo_red_dark))
            // Ask first
            Snackbar.make(view, getString(R.string.stop_warning), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.ok)) {
                    // Stop timer, change icon
                    sharedModel.match.stop()
                    view.lockButtons(true)
                    this.visibility = View.GONE
                    view.findViewById<FloatingActionButton>(R.id.fab_match_timer).visibility = View.GONE

                    // Display 'END'
                    val startTime = sharedModel.match.displayTime(sharedModel.match.startTime)
                    val endTime = sharedModel.match.displayTime(sharedModel.match.endTime)
                    val elapsedTime = sharedModel.match.displayElapsedTime()
                    val msg = "${getString(R.string.time_end)}: $matchType ($startTime - $endTime | $elapsedTime)"
                    Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                }
                .addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
                    override fun onShown(transientBottomBar: Snackbar?) {
                        super.onShown(transientBottomBar)
                        this@initFloatingStopTimer.setColorFilter(v.context.getColor(android.R.color.holo_red_dark))
                    }

                    override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                        super.onDismissed(transientBottomBar, event)
                        this@initFloatingStopTimer.setColorFilter(v.context.getColor(android.R.color.white))
                    }
                })
                .setActionTextColor(getColor(context, android.R.color.holo_orange_dark))
                .show()
            return@setOnLongClickListener true
        }
    }

    private fun EditText.initPlayerNameEditText(i: Int, view: View) {
        this.doAfterTextChanged { playerName ->
            sharedModel.match.renamePlayer(i, playerName.toString())
        }
        this.setText(getNumberedPlayer(view, i))
    }

    private fun ImageView.initPlayer1SYes(player: Player) {
        this.setOnClickListener { player.addServe(Player.Serve.FIRST, true) }
    }
    private fun ImageView.initPlayer1SNo(player: Player) {
        this.setOnClickListener { player.addServe(Player.Serve.FIRST, false) }
    }
    private fun ImageView.initPlayer1SUndo(player: Player) {
        this.setOnClickListener { player.undoServe(Player.Serve.FIRST) }
    }
    private fun ImageView.initPlayer2SYes(player: Player) {
        this.setOnClickListener { player.addServe(Player.Serve.SECOND, true) }
    }
    private fun ImageView.initPlayer2SNo(player: Player) {
        this.setOnClickListener { player.addServe(Player.Serve.SECOND, false) }
    }
    private fun ImageView.initPlayer2SUndo(player: Player) {
        this.setOnClickListener { player.undoServe(Player.Serve.SECOND) }
    }
    private fun ImageView.initPlayerWin(player: Player) {
        this.setOnClickListener { player.addWin() }
    }

    /**
     * It will no longer be possible to tap button.
     */
    private fun View.lockButtons(lock: Boolean) {
        playerViews.forEach { playerInfo ->
            playerInfo.findViewById<ImageView>(R.id.player_1S_yes).isEnabled = !lock
            playerInfo.findViewById<ImageView>(R.id.player_1S_no).isEnabled = !lock
            playerInfo.findViewById<ImageView>(R.id.player_1S_undo).isEnabled = !lock

            playerInfo.findViewById<ImageView>(R.id.player_2S_yes).isEnabled = !lock
            playerInfo.findViewById<ImageView>(R.id.player_2S_no).isEnabled = !lock
            playerInfo.findViewById<ImageView>(R.id.player_2S_undo).isEnabled = !lock

            playerInfo.findViewById<ImageView>(R.id.player_win).isEnabled = !lock
        }
    }

}



