package cz.muni.tournamentstats.players

import java.text.NumberFormat
import java.util.*

data class Player(var name:String = "Player", var team: Team) {
    var id:Long = -1
    override fun toString(): String = name

    enum class Serve {
        FIRST, SECOND
    }
    enum class Team {
        A, B
    }

    // Stats
    private val serve1 = mutableListOf<Boolean>()
    private val serve2 = mutableListOf<Boolean>()
    var accuracy1 = 0.0
        private set
    var accuracy2 = 0.0
        private set
    val doubleFaults : Int
        get() = serve2.count {!it} // FALSE in serve2
    var winningBalls : Int = 0
        private set

    var visible : Boolean = true
    private val numberFormat:NumberFormat = NumberFormat.getNumberInstance(Locale.getDefault()).apply {
        maximumFractionDigits = 2
    }

    /**
     * Converts [accuracy1] or [accuracy2] to percentage, rounded to two decimal places.
     */
    fun displayAccuracyPercentage(number: Serve): String {
        var accuracy = when (number) {
            Serve.FIRST -> accuracy1
            Serve.SECOND -> accuracy2
        }
        accuracy *= 100
        return numberFormat.format(accuracy) + "%"
    }

    /**
     * Returns the actual [accuracy1] or [accuracy2] numbers.
     */
    fun displayAccuracyCount(number: Serve, separator: Char = '/') : String {
        val serves = getServes(number)
        return "${serves.count {it}}$separator${serves.size}"
    }


    /**
     * Undoes the last recorded action in [serve1] or [serve2].
     */
    fun undoServe(number: Serve) {
        val serves = getServes(number)
        if (serves.size > 0) {
            serves.removeAt(serves.size - 1)
            updateAccuracy(number)
        }
    }

    /**
     * Records if [serve1] or [serve2] was successful.
     */
    fun addServe(number: Serve, success: Boolean) {
        getServes(number).add(success)
        updateAccuracy(number)
    }

    /**
     * Adds a winning ball(s) to the player.
     */
    fun addWin(amount:Int = 1) {
        winningBalls += amount
    }

    /**
     * Gets the proper [serve1] or [serve2] list based on the [number].
     */
    private fun getServes(number: Serve) : MutableList<Boolean> {
        return when (number) {
            Serve.FIRST -> serve1
            Serve.SECOND -> serve2
        }
    }

    /**
     * Updates accuracy stats for both [serve1] and [serve2], or only one of them.
     */
    private fun updateAccuracy(number: Serve? = null) {
        when (number) {
            null -> {
                accuracy1 = calculateAccuracy(serve1)
                accuracy2 = calculateAccuracy(serve2)
            }
            Serve.FIRST -> accuracy1 = calculateAccuracy(serve1)
            Serve.SECOND -> accuracy2 = calculateAccuracy(serve2)
        }
    }

    /**
     * Calculates the accuracy value from the list of successes.
     */
    private fun calculateAccuracy(serves: MutableList<Boolean>) : Double {
        if (serves.size == 0) return 0.0
        return serves.count {it} / serves.size.toDouble()
    }

}