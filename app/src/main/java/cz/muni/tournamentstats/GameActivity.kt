package cz.muni.tournamentstats


import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import cz.muni.tournamentstats.database.DbHandler
import cz.muni.tournamentstats.matches.tabs.PlayerFragment
import cz.muni.tournamentstats.matches.tabs.StatFragment
import cz.muni.tournamentstats.ui.main.SectionsPagerAdapter

class GameActivity : AppCompatActivity() {

    // Tab Layout with Different Fragments | https://youtu.be/h4HwU_ENXYM?list=WL
    // https://developer.android.com/training/basics/fragments/pass-data-between

    lateinit var dbHandler: DbHandler

    private lateinit var playerFragment: PlayerFragment
    private lateinit var statFragment: StatFragment

    private var backPressed: Boolean = false // Double-tap to exit

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        val doubles = intent.getBooleanExtra(getString(R.string.doublesMatchKey), false)

        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager, doubles)
        val viewPager = findViewById<ViewPager>(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        playerFragment = sectionsPagerAdapter.getItem(0) as PlayerFragment
        statFragment = sectionsPagerAdapter.getItem(1) as StatFragment

        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)
    }

    override fun onBackPressed() {
        // Confirm to quit
        Snackbar.make(findViewById(android.R.id.content), getString(R.string.back_warning), Snackbar.LENGTH_LONG)
            //.setAction("Action", null).show()
            .setAction(getString(R.string.ok), View.OnClickListener {
                super.onBackPressed()
            })
            .setActionTextColor(getColor(android.R.color.holo_red_light))
            .show();

    }
}