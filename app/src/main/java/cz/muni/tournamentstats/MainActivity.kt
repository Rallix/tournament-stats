package cz.muni.tournamentstats

import android.content.Intent
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.time.ExperimentalTime

class MainActivity : AppCompatActivity() {

    // Tab Layout with Different Fragments | https://youtu.be/h4HwU_ENXYM?list=WL
    // https://developer.android.com/training/basics/fragments/pass-data-between

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_MatchSingles.setOnClickListener {
            val intent = Intent(this, GameActivity::class.java).apply {
                putExtra(getString(R.string.doublesMatchKey), false)
            }
            startActivity(intent)
        }

        btn_MatchDoubles.setOnClickListener {
            val intent = Intent(this, GameActivity::class.java).apply {
                putExtra(getString(R.string.doublesMatchKey), true)
            }
            startActivity(intent)
        }

        // TODO: History
        /*
        btn_MatchHistory.setOnClickListener { view ->
            Snackbar.make(view, "Historie zápasů zatím nefunguje.", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }*/
    }
}