package cz.muni.tournamentstats.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class PageViewModel : ViewModel() {

    private val _index = MutableLiveData<Int>()
    val text: LiveData<String> = Transformations.map(_index) {
        "Welcome to the section: $it"
    }

    /** Add different fragments for each activity */

    fun setIndex(index: Int) {
        _index.value = index
    }
}